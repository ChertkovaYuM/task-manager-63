package ru.tsc.chertkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

public final class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest() {
    }

    public DataBackupLoadRequest(@Nullable String token) {
        super(token);
    }

}
