package ru.tsc.chertkova.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.dto.model.AbstractModelDTO;

@Repository
public interface AbstractDtoRepository<M extends AbstractModelDTO>
        extends JpaRepository<M, String> {

}
