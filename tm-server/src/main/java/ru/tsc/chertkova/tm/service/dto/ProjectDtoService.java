package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.service.dto.IProjectDtoService;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.StatusNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.chertkova.tm.repository.dto.TaskDtoRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectDtoService extends AbstractUserOwnerDtoService<ProjectDTO> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO add(@Nullable final ProjectDTO project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(project.getUserId()).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(project.getName()).orElseThrow(NameEmptyException::new);
        projectRepository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String id,
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @Nullable ProjectDTO project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        projectRepository.saveAndFlush(project);
        project = findById(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        projectRepository.changeStatus(id, userId, status.getDisplayName());
        @Nullable ProjectDTO project = findById(userId, id);
        return project;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return projectRepository.existsById(id);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO findById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable ProjectDTO project = projectRepository.findById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final ProjectDTO project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        projectRepository.removeById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO remove(
            @Nullable final String userId,
            @Nullable final ProjectDTO project
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(findById(project.getUserId(), project.getId()))
                .orElseThrow(ProjectNotFoundException::new);
        removeById(project.getUserId(), project.getId());
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        int size = projectRepository.getSize(userId);
        return size;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @Nullable List<ProjectDTO> projects = projectRepository.findAll(userId);
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<ProjectDTO> addAll(@NotNull final List<ProjectDTO> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        projectRepository.saveAll(projects);
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<ProjectDTO> removeAll(@Nullable final List<ProjectDTO> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        projectRepository.deleteAll(projects);
        return projects;
    }

    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final TaskDtoRepository taskRepository = context.getBean(TaskDtoRepository.class);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        Optional.ofNullable(taskRepository.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        taskRepository.bindTaskToProject(taskId, projectId, userId);
        @Nullable TaskDTO task = taskRepository.findById(userId, taskId);
        return task;
    }

}
