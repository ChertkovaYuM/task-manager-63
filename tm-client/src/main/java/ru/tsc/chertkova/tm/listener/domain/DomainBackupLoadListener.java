package ru.tsc.chertkova.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.data.DataBackupLoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractDomainListener;

@Component
public final class DomainBackupLoadListener extends AbstractDomainListener {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup from file.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainBackupLoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        getDomainEndpoint().loadDataBackup(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
